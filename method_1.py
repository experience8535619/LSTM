# -*- coding: utf-8 -*-
"""
Created on Tue May 11 10:25:11 2021

@author: FJUSER200921H
"""


import numpy as np
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense, Dropout
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.preprocessing import StandardScaler
import seaborn as sns
import talib
#======================================================================================================
#============================================{  Preprocessing  }=======================================
#======================================================================================================
#------------------------------------------------------------------------------------------------------
# import data
df = pd.read_csv("C:/Users/FJUSER200921H/Desktop/Youtube/TXF1 1 分鐘.csv")
print(df.head())
df.columns = ['Date', 'Time', 'Open', 'High', 'Low', 'Close', 'Volume']
print(df.head())

#------------------------------------------------------------------------------------------------------
# generate SAR 
df['SAR'] = talib.SAR(df.High, df.Low)
print(df.head())
df = df.dropna()
print(df.head())

#------------------------------------------------------------------------------------------------------
#Separate dates for future plotting
df['Date'] = df['Date']+" "+ df['Time']
print(df.head()) 
df = df.drop(labels = ['Time'],axis = 1)
print(df.head())
df['Date'] = pd.to_datetime(df.Date, format = '%Y/%m/%d %H:%M:%S')
print(df.head())

# divide the data into the range we want to predict
#df = df.loc[df['Date']==2019]
predict_date = '2019-09-09 08:45:00'                             #******************************
predict_date_end = '2019-09-09 13:45:00'                         #******************************
test_data = df.loc[df['Date'] >= predict_date]
test_data = test_data.loc[test_data['Date'] <= predict_date_end]

df = df.loc[df['Date']>='2019-08-01 00']
df = df.loc[df['Date'] < predict_date]
df = df.reset_index(drop=True)
print(df.head())


train_dates = pd.to_datetime(df['Date'])
print(df.head())


#------------------------------------------------------------------------------------------------------
#plot the entire trend
'''
#plot with plotly
import plotly.graph_objects as go
import plotly.io as pio
#pio.renderers.default = 'svg'
pio.renderers.default = 'browser'

#df = df.loc[df['Date'] > '2019']
df = df.loc[df['Date'] >= '2020']
_title = 'History Price of TXF1 in 1 Minute Time Range'

fig = go.Figure()

fig.add_trace(go.Scatter(x=df.Date, y=df.Open, name='Real Track',
                         line=dict(color='firebrick', width=1.2)))

fig.update_xaxes(
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1day", step="day", stepmode="backward"),
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=3, label="3m", step="month", stepmode="backward"),
            dict(step="all")
        ])
    ),
    rangeslider_visible=True,
    rangebreaks=[
        dict(bounds=["sat", "mon"]), #hide weekends
        dict(bounds=[13.75, 8.75], pattern="hour"), #hide hours outside of 8:45am-13:45pm
    ]
)
fig.update_layout(
    title=_title
)
fig.show()
fig.write_html("History_Price_fig.html")

'''
#------------------------------------------------------------------------------------------------------
#Variables for training
cols = list(df)[1:7]
print(cols)

#------------------------------------------------------------------------------------------------------
#transform all types to float
df_for_training = df[cols].astype(float)
print(df_for_training.head())


#------------------------------------------------------------------------------------------------------
#add '三分鐘收最高' signal
max_len = 3
signal_3=[]
for i in range(len(df_for_training)):
    if i >= max_len:
        if df_for_training.at[i,'Close'] >= max(df_for_training.loc[i-max_len:i,'Close']):
            signal_3.append(1)
        else:
            signal_3.append(0)
    else:
        signal_3.append(0)
        
df_for_training['signal_3'] = signal_3
print(df_for_training.head())

#------------------------------------------------------------------------------------------------------
#add '五分鐘收最高' signal
max_len = 5
signal_5=[]
for i in range(len(df_for_training)):
    if i >= max_len:
        if df_for_training.at[i,'Close'] >= max(df_for_training.loc[i-max_len:i,'Close']):
            signal_5.append(1)
        else:
            signal_5.append(0)
    else:
        signal_5.append(0)
        
df_for_training['signal_5'] = signal_5
print(df_for_training.head())

#------------------------------------------------------------------------------------------------------
#add '六分鐘收最高' signal
max_len = 6
signal_6=[]
for i in range(len(df_for_training)):
    if i >= max_len:
        if df_for_training.at[i,'Close'] >= max(df_for_training.loc[i-max_len:i,'Close']):
            signal_6.append(1)
        else:
            signal_6.append(0)
    else:
        signal_6.append(0)
        
df_for_training['signal_6'] = signal_6
print(df_for_training.head())

#------------------------------------------------------------------------------------------------------
# add BBand
upper, middle, lower = talib.BBANDS(df.Close, 
                                    timeperiod=5, 
                                    nbdevup=2.1, 
                                    nbdevdn=2.1, 
                                    # Moving average type: simple moving average here
                                    matype=0)
df_for_training['upper'] = upper
df_for_training['middle'] = middle
df_for_training['lower'] = lower
print(df_for_training.head(30))

df_for_training.drop(df_for_training.head(4).index,inplace=True) # drop first n rows
print(df_for_training.head())
train_dates.drop(train_dates.head(4).index,inplace=True) # drop first n rows
print(train_dates.head())

#------------------------------------------------------------------------------------------------------
#transform all types to float
df_for_training = df_for_training.astype(float)
print(df_for_training.head())

#======================================================================================================
#============================================{  Training  }============================================
#======================================================================================================
#------------------------------------------------------------------------------------------------------
#LSTM uses sigmoid and tanh that are sensitive to magnitude so values need to be normalized
# normalize the dataset
scaler = StandardScaler()
scaler = scaler.fit(df_for_training)
df_for_training_scaled = scaler.transform(df_for_training)

#------------------------------------------------------------------------------------------------------
#As required for LSTM networks, we require to reshape an input data into n_samples x timesteps x n_features. 
trainX = []
trainY = []

n_future = 1   # Number of minutes we want to predict into the future
n_past = 5    # Number of past minutes we want to use to predict the future

for i in range(n_past, len(df_for_training_scaled) - n_future +1):
    trainX.append(df_for_training_scaled[i - n_past:i, 0:df_for_training.shape[1]])
    trainY.append(df_for_training_scaled[i + n_future - 1:i + n_future, 0])

trainX, trainY = np.array(trainX), np.array(trainY)

print('trainX shape == {}.'.format(trainX.shape))
print('trainY shape == {}.'.format(trainY.shape))
print(df_for_training.head())
print(df_for_training.columns)

#------------------------------------------------------------------------------------------------------
# define Autoencoder model
model = Sequential()
model.add(LSTM(64, activation='relu', input_shape=(trainX.shape[1], trainX.shape[2]), return_sequences=False))
#model.add(LSTM(32, activation='relu', return_sequences=False))
model.add(Dropout(0.2)) #0.5
#
model.add(Dense(trainY.shape[1]))

model.compile(optimizer='adam', loss='mse')
model.summary()

#------------------------------------------------------------------------------------------------------
# fit model
#batch_size=10
#epochs=100
history = model.fit(trainX, trainY, epochs=100, batch_size=16, validation_split=0.1, verbose=1)

#------------------------------------------------------------------------------------------------------
#save trained model
model.save('9_09.h5')  # creates a HDF5 file 'lstm_model_1.h5'   #******************************
#del model  # deletes the existing model

#------------------------------------------------------------------------------------------------------
# returns a compiled model
#from keras.models import load_model
# identical to the previous one
#model = load_model('lstm_model_1.h5')

#------------------------------------------------------------------------------------------------------
# list all data in history
print(history.history.keys())
plt.plot(history.history['loss'], label='Training loss')
plt.plot(history.history['val_loss'], label='Validation loss')
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend()
print('Training loss: ',history.history['loss'][-1])
print('Validation loss: ',history.history['val_loss'][-1])
mse = pd.DataFrame({'Training loss':[history.history['loss'][-1]], 'Validation loss':[history.history['val_loss'][-1]]})


mse.to_csv('9_09_mse.csv')                                        #******************************


#======================================================================================================
#============================================{  Forecasting  }=========================================
#======================================================================================================
#------------------------------------------------------------------------------------------------------
#Forecasting...
#Start with the last day in training date and predict future...
n_future=300  #Redefining n_future to extend prediction dates beyond original n_future dates...
forecast_period_dates = pd.date_range(predict_date, periods=n_future, freq='min').tolist()
forecast = model.predict(trainX[-n_future:]) #forecast 
train_forecast = model.predict(trainX[n_past:]) #forecast 

#------------------------------------------------------------------------------------------------------
#Perform inverse transformation to rescale back to original range
forecast_copies = np.repeat(forecast, df_for_training.shape[1], axis=-1)
y_pred_future = scaler.inverse_transform(forecast_copies)[:,0]
print(len(y_pred_future))
train_forecast_copies = np.repeat(train_forecast, df_for_training.shape[1], axis=-1)
y_pred_train = scaler.inverse_transform(train_forecast_copies)[:,0]
print(len(y_pred_train))

#======================================================================================================
#============================================{  Plotting  }============================================
#======================================================================================================
#------------------------------------------------------------------------------------------------------
# Convert timestamp to date
forecast_dates = []
for time_i in forecast_period_dates:
    forecast_dates.append(time_i)


#------------------------------------------------------------------------------------------------------
# plot with seaborn    

df_forecast = pd.DataFrame({'Date':np.array(forecast_dates), 'Open':y_pred_future, 'Real': test_data['Open']})
df_forecast['Date']=pd.to_datetime(df_forecast['Date'])
print(len(df_forecast))

print(len(train_dates))
print(len(df_for_training))
original = pd.DataFrame({'Date':np.array(train_dates), 'Open':df_for_training['Open']})
original['Date']=pd.to_datetime(original['Date'])

original.drop(original.head(10).index,inplace=True)
original['Pred'] = y_pred_train
#original = original.loc[original['Date'] >= '2020-12-21 20']
#original = original.loc[original['Date'] >= '2020-10-18 20']
#original = original.loc[original['Date'] >= '2020-10-18 00']

plt.xticks(rotation=45)
sns.lineplot(original['Date'], original['Open'])
sns.lineplot(df_forecast['Date'], df_forecast['Open'])
sns.lineplot(original['Date'], original['Pred'])

#------------------------------------------------------------------------------------------------------
# plot with matplotlib
# Set plot size 
from pylab import rcParams
rcParams['figure.figsize'] = 14, 5

plt.plot(df_forecast['Date'], df_forecast['Open'], color='r', label='Predicted Stock Price')
plt.plot(original['Date'], original['Pred'], color='orange', label='Training predictions')
plt.plot(original['Date'], original['Open'], color='b', label='Actual Stock Price')

plt.axvline(x = max(list(train_dates)), color='green', linewidth=2, linestyle='--')

plt.grid(which='major', color='#cccccc', alpha=0.5)

plt.legend(shadow=True)
plt.title('Predcitions and Acutal Stock Prices', family='Arial', fontsize=12)
plt.xlabel('Timeline', family='Arial', fontsize=10)
plt.ylabel('Stock Price Value', family='Arial', fontsize=10)
plt.xticks(rotation=45, fontsize=8)
plt.show()

#------------------------------------------------------------------------------------------------------

#plot with plotly
import plotly.graph_objects as go
import plotly.io as pio
#pio.renderers.default = 'svg'
pio.renderers.default = 'browser'

#original['index'] = signal.index
#original = original.set_index('index')
#original['sig'] = signal
_title = 'Predcitions and Acutal Stock Prices'

#df_forecast.Real = pd.DataFrame(test_data['Open'])


fig = go.Figure()

fig.add_trace(go.Scatter(x=original.Date, y=original.Open, name='Real Track',
                         line=dict(color='firebrick', width=1.2)))

fig.add_trace(go.Scatter(x=df_forecast.Date, y=df_forecast.Real, name = 'Real Track',
                         line=dict(color='firebrick', width=1.2)))
fig.add_trace(go.Scatter(x=df_forecast.Date, y=df_forecast.Open, name = 'Prediction',
                         line=dict(color='royalblue', width=1.2)))

fig.update_xaxes(
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1day", step="day", stepmode="backward"),
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=3, label="3m", step="month", stepmode="backward"),
            dict(step="all")
        ])
    ),
    rangeslider_visible=True,
    rangebreaks=[
        dict(bounds=["sat", "mon"]), #hide weekends
        dict(bounds=[13.75, 8.75], pattern="hour"), #hide hours outside of 8:45am-13:45pm
    ]
)
fig.update_layout(
    title=_title
)
fig.show()
fig.write_html("9_09_fig.html")                          #******************************
#-----------------------------------------------------------------------------------------------------

#plot with plotly
import plotly.graph_objects as go
import plotly.io as pio
#pio.renderers.default = 'svg'
pio.renderers.default = 'browser'

#original['index'] = signal.index
#original = original.set_index('index')
#original['sig'] = signal
_title = 'Predcitions and Acutal Stock Prices'

#df_forecast.Real = pd.DataFrame(test_data['Open'])


fig = go.Figure()

fig.add_trace(go.Scatter(x=original.Date, y=original.Open, name='Real Track',
                         line=dict(color='rgb(0,0,0)', width=1.2)))

fig.add_trace(go.Scatter(x=df_forecast.Date, y=df_forecast.Real, name = 'Real Track',
                         line=dict(color='rgb(0,0,0)', width=1.2)))
fig.add_trace(go.Scatter(x=df_forecast.Date, y=df_forecast.Open, name = 'Prediction',
                         line=dict(color='rgb(160,160,160)', width=1.2)))

fig.update_xaxes(
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1day", step="day", stepmode="backward"),
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=3, label="3m", step="month", stepmode="backward"),
            dict(step="all")
        ])
    ),
    rangeslider_visible=True,
    rangebreaks=[
        dict(bounds=["sat", "mon"]), #hide weekends
        dict(bounds=[13.75, 8.75], pattern="hour"), #hide hours outside of 8:45am-13:45pm
    ]
)
fig.update_layout(
    title=_title
)
fig.show()
fig.write_html("9_09_gray_fig.html")                          #******************************

#======================================================================================================
#============================================{  Trading Strategy  }====================================
#======================================================================================================
#------------------------------------------------------------------------------------------------------
# Strategy 1
# buy / sell criteria
sig = []
pos = []
#print(signals)
print(df_forecast.tail())
print(min(df_forecast.index))
for i in range(min(df_forecast.index),max(df_forecast.index)+1):
    # 當天買進 或 放空 時點
    if int(df_forecast.at[i,'Date'].hour) == 9 and int(df_forecast.at[i,'Date'].minute) == 0 and int(df_forecast.at[i,'Date'].second) == 0:
        print(df_forecast.at[i,'Date'])
        if df_forecast.at[i,'Open'] < df_forecast.at[i+255,'Open']:#漲
            sig.append(1)
            pos.append(1)
        elif df_forecast.at[i,'Open'] > df_forecast.at[i+255,'Open']:#跌
            sig.append(-1)
            pos.append(-1)
        else:    #不漲不跌
            sig.append(0)
    # 當天平倉時點
    elif int(df_forecast.at[i,'Date'].hour) == 13 and int(df_forecast.at[i,'Date'].minute) >= 15 and int(df_forecast.at[i,'Date'].second) >= 0:
        if pos[-1] == 1:
            sig.append(-1)
            pos.append(0)
        elif pos[-1] == -1:
            sig.append(1)
            pos.append(0)
        else:
            sig.append(0)
    else:
        sig.append(0)
                
print(sig)
signals = pd.DataFrame({"signal":sig})
print(signals)
signals['positions'] = signals['signal'].diff()
print(signals)
print(len(df_forecast))
#------------------------------------------------------------------------------------------------------
# Strategy 2
# buy / sell criteria
sig = []
pos = []
#print(signals)
print(df_forecast.tail())
print(min(df_forecast.index))
print(list(df_forecast['Open']))
min_point = min(list(df_forecast['Open']))
max_point = max(list(df_forecast['Open']))
for i in range(min(df_forecast.index),max(df_forecast.index)+1):
    # 當天買進 或 放空 時點
    if df_forecast.at[i,'Open'] == min_point:
        sig.append(1)
        pos.append(1)
    elif df_forecast.at[i,'Open'] == max_point:
        sig.append(-1)
        pos.append(-1)
    else:
        sig.append(0)
    
                
print(sig)
signals = pd.DataFrame({"signal":sig})
print(signals)
signals['positions'] = signals['signal'].diff()
print(signals)
print(len(df_forecast))
print(len(signals))

#------------------------------------------------------------------------------------------------------
# implementation
real_movement = df_forecast['Real']
real_movement =pd.DataFrame(real_movement)
signal = signals['signal']
initial_money = 10000000
max_buy = 1
max_sell = 1

starting_money = initial_money
states_sell = []
states_buy = []
current_inventory = 0

s = 0
b = 0
a = [i for i in range(len(df_forecast))]
print(a)
real_movement['index'] = a
print(real_movement.head())
real_movement = real_movement.set_index('index')
print(real_movement.at[0,'Real'])
print(signal)
def buy(b, i, initial_money, current_inventory):
        shares = initial_money // real_movement.at[i,'Real']
        if shares < 1:
            print(
                'day %d: total balances %f, not enough money to buy a unit price %f'
                % (i, initial_money, real_movement.at[i,'Real'])
            )
        else:
            if shares > max_buy:
                buy_units = max_buy
            else:
                buy_units = shares
            initial_money -= buy_units * real_movement.at[i,'Real']
            current_inventory += buy_units
            b += 1
            print(
                'day %d: buy %d units at price %f, total balance %f'
                % (i, buy_units, buy_units * real_movement.at[i,'Real'], initial_money)
            )
            print('current_inventory', current_inventory)
            #states_buy.append(0)
        return b, initial_money, current_inventory
    
print(signal.iloc[1])
for i in range(len(real_movement)):
    state = signal.iloc[i]
    if state == 1:
        b, initial_money, current_inventory = buy(
            b, i, initial_money, current_inventory
        )
        states_buy.append(i)
    elif state == -1:
        #if current_inventory == 0:
                #print('day %d: cannot sell anything, inventory 0' % (i))
        #else:
        if current_inventory > max_sell:
            sell_units = max_sell
        else:
            #sell_units = current_inventory
            sell_units = 1
        current_inventory -= sell_units
        total_sell = sell_units * real_movement.at[i,'Real']
        initial_money += total_sell
        try:
            invest = (
                (real_movement.at[i,'Real'] - real_movement.at[states_buy[-1],'Real'])
                / real_movement.at[states_buy[-1],'Real']
            ) * 100
        except:
            invest = 0
        s += 1
        print(
            'day %d, sell %d units at price %f, investment %f %%, total balance %f,'
            % (i, sell_units, total_sell, invest, initial_money)
        )
        print('current_inventory', current_inventory)
        states_sell.append(i)
invest = ((initial_money - starting_money) / starting_money) * 100
total_gains = initial_money - starting_money

print(total_gains)
print(current_inventory)
print(s)
print(b)
print(invest)
print(states_buy)
print(states_sell)

#======================================================================================================
#============================================{  Plot the Result  }====================================-
#======================================================================================================
#------------------------------------------------------------------------------------------------------

'''
# plot with matplotlib
# Set plot size 
from pylab import rcParams
rcParams['figure.figsize'] = 14, 5

#original = original.set_index('index')
plt.plot(df_forecast['Date'], df_forecast['Open'], color='r', label='Predicted Stock Price')
plt.plot(original['Date'], original['Pred'], color='orange', label='Training predictions')
plt.plot(original['Date'], original['Open'], color='b', label='Actual Stock Price')

plt.axvline(x = max(list(train_dates)), color='green', linewidth=2, linestyle='--')

plt.grid(which='major', color='#cccccc', alpha=0.5)

plt.legend(shadow=True)
#plt.title('Predcitions and Acutal Stock Prices', family='Arial', fontsize=12)
plt.xlabel('Timeline', family='Arial', fontsize=10)
plt.ylabel('Stock Price Value', family='Arial', fontsize=10)
plt.xticks(rotation=45, fontsize=8)
plt.plot(original['Date'], original['Open'], '^', markersize=10, color='m', label = 'buying signal', markevery = states_buy)
plt.plot(original['Date'], original['Open'], 'v', markersize=10, color='k', label = 'selling signal', markevery = states_sell)
plt.title('total gains %f, total investment %f%%'%(total_gains, invest))
plt.legend()
plt.show()
'''
#------------------------------------------------------------------------------------------------------
#plot with plotly
import plotly.graph_objects as go
import plotly.io as pio
#pio.renderers.default = 'svg'
pio.renderers.default = 'browser'

df_forecast['index'] = signal.index
df_forecast = df_forecast.set_index('index')
df_forecast['sig'] = signal
#df_forecast = pd.concat([df_forecast, signal], axis=1)
#signal = signal.astype(float)
#df_forecast['sig'] = signal
_title = 'total gains %f, total investment %f%%'%(total_gains, invest)

#df_forecast.Real = pd.DataFrame(test_data['Open'])


fig = go.Figure()

fig.add_trace(go.Scatter(x=original.Date, y=original.Open, name='Real Track',
                         line=dict(color='firebrick', width=1.2)))

fig.add_trace(go.Scatter(x=df_forecast.Date, y=df_forecast.Real, name = 'Real Track',
                         line=dict(color='firebrick', width=1.2)))
fig.add_trace(go.Scatter(x=df_forecast.Date, y=df_forecast.Open, name = 'Prediction',
                         line=dict(color='royalblue', width=1.2)))
fig.add_trace(go.Scatter(
    name="Buy",
    mode="markers", x=[df_forecast.at[i,'Date'] for i in range(len(df_forecast)) if df_forecast.at[i,'sig'] == 1], y=[df_forecast.at[i,'Real']+10 for i in range(len(df_forecast)) if df_forecast.at[i,'sig'] == 1],
    marker=dict(
        size=8,
        color= 'rgb(255,0,0)',
        symbol='triangle-down'
    )
))
fig.add_trace(go.Scatter(
    name="Sell",
    mode="markers", x=[df_forecast.at[i,'Date'] for i in range(len(df_forecast)) if df_forecast.at[i,'sig'] == -1], y=[df_forecast.at[i,'Real']-10 for i in range(len(df_forecast)) if df_forecast.at[i,'sig'] == -1],
    marker=dict(
        size=8,
        color= 'rgb(25,0,0)',
        symbol='triangle-up'
    )
))
'''x1 = [df_forecast.at[i,'Date'] for i in range(len(df_forecast)) if df_forecast.at[i,'Date'].hour == 9 and df_forecast.at[i,'sig'] != 0]
y1 = [df_forecast.at[i,'Real']+60 for i in range(len(df_forecast)) if (df_forecast.at[i,'Date'].hour == 9 and df_forecast.at[i,'sig'] != 0)]
fig.add_trace(go.Scatter(
    name="Day Start",
    mode="markers", x=x1, y=y1,
    marker=dict(
        size=8,
        color= 'rgb(247,216,37)',
        symbol='diamond-tall'
    )
))'''
fig.update_xaxes(
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1day", step="day", stepmode="backward"),
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=3, label="3m", step="month", stepmode="backward"),
            dict(step="all")
        ])
    ),
    rangeslider_visible=True,
    rangebreaks=[
        dict(bounds=["sat", "mon"]), #hide weekends
        dict(bounds=[13.75, 8.75], pattern="hour"), #hide hours outside of 8:45am-13:45pm
    ]
)
fig.update_layout(
    title=_title
)
fig.show()
fig.write_html("9_09_strat_2_fig.html")                 #******************************

#------------------------------------------------------------------------------------------------------
#plot with plotly
import plotly.graph_objects as go
import plotly.io as pio
#pio.renderers.default = 'svg'
pio.renderers.default = 'browser'

df_forecast['index'] = signal.index
df_forecast = df_forecast.set_index('index')
df_forecast['sig'] = signal
#df_forecast = pd.concat([df_forecast, signal], axis=1)
#signal = signal.astype(float)
#df_forecast['sig'] = signal
_title = 'total gains %f, total investment %f%%'%(total_gains, invest)

#df_forecast.Real = pd.DataFrame(test_data['Open'])


fig = go.Figure()

fig.add_trace(go.Scatter(x=original.Date, y=original.Open, name='Real Track',
                         line=dict(color='rgb(0,0,0)', width=1.5)))

fig.add_trace(go.Scatter(x=df_forecast.Date, y=df_forecast.Real, name = 'Real Track',
                         line=dict(color='rgb(0,0,0)', width=1.5)))
fig.add_trace(go.Scatter(x=df_forecast.Date, y=df_forecast.Open, name = 'Prediction',
                         line=dict(color='rgb(160,160,160)', width=1.5)))
fig.add_trace(go.Scatter(
    name="Buy",
    mode="markers", x=[df_forecast.at[i,'Date'] for i in range(len(df_forecast)) if df_forecast.at[i,'sig'] == 1], y=[df_forecast.at[i,'Real']+10 for i in range(len(df_forecast)) if df_forecast.at[i,'sig'] == 1],
    marker=dict(
        size=10,
        color= 'rgb(0,0,0)',
        symbol='triangle-down'
    )
))
fig.add_trace(go.Scatter(
    name="Sell",
    mode="markers", x=[df_forecast.at[i,'Date'] for i in range(len(df_forecast)) if df_forecast.at[i,'sig'] == -1], y=[df_forecast.at[i,'Real']-10 for i in range(len(df_forecast)) if df_forecast.at[i,'sig'] == -1],
    marker=dict(
        size=10,
        color= 'rgb(0,0,0)',
        symbol='triangle-up'
    )
))
'''
x1 = [df_forecast.at[i,'Date'] for i in range(len(df_forecast)) if df_forecast.at[i,'Date'].hour == 9 and df_forecast.at[i,'sig'] != 0]
y1 = [df_forecast.at[i,'Real']+10 for i in range(len(df_forecast)) if (df_forecast.at[i,'Date'].hour == 9 and df_forecast.at[i,'sig'] != 0)]
fig.add_trace(go.Scatter(
    name="Day Start",
    mode="markers", x=x1, y=y1,
    marker=dict(
        size=8,
        color= 'rgb(247,216,37)',
        symbol='diamond-tall'
    )
))'''
fig.update_xaxes(
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1day", step="day", stepmode="backward"),
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=3, label="3m", step="month", stepmode="backward"),
            dict(step="all")
        ])
    ),
    rangeslider_visible=True,
    rangebreaks=[
        dict(bounds=["sat", "mon"]), #hide weekends
        dict(bounds=[13.75, 8.75], pattern="hour"), #hide hours outside of 8:45am-13:45pm
    ]
)
fig.update_layout(
    title=_title
)
fig.show()
fig.write_html("9_09_strat_2_gray_fig.html")                 #******************************

#-------------------------------------------------------------------------------------------------------
'''
#plot with plotly
import plotly.graph_objects as go
import plotly.io as pio
#pio.renderers.default = 'svg'
pio.renderers.default = 'browser'

original['index'] = signal.index
original = original.set_index('index')
original['sig'] = signal
_title = 'total gains %f, total investment %f%%'%(total_gains, invest)

fig = go.Figure()

fig.add_trace(go.Scatter(x=original.Date, y=original.Open, name='Real Track',
                         line=dict(color='firebrick', width=1.2)))

fig.add_trace(go.Scatter(x=original.Date, y=original.Pred, name = 'Prediction',
                         line=dict(color='royalblue', width=1.2)))
fig.add_trace(go.Scatter(
    name="Buy",
    mode="markers", x=[original.at[i,'Date'] for i in range(len(original)) if original.at[i,'sig'] == 1], y=[original.at[i,'Open'] for i in range(len(original)) if original.at[i,'sig'] == 1],
    marker=dict(
        size=8,
        color= 'rgb(255,0,0)',
        symbol='triangle-down'
    )
))
fig.add_trace(go.Scatter(
    name="Sell",
    mode="markers", x=[original.at[i,'Date'] for i in range(len(original)) if original.at[i,'sig'] == -1], y=[original.at[i,'Open'] for i in range(len(original)) if original.at[i,'sig'] == -1],
    marker=dict(
        size=8,
        color= 'rgb(25,0,0)',
        symbol='triangle-up'
    )
))
x1 = [original.at[i,'Date'] for i in range(len(original)) if original.at[i,'Date'].hour == 9 and original.at[i,'sig'] != 0]
y1 = [original.at[i,'Open']+10 for i in range(len(original)) if (original.at[i,'Date'].hour == 9 and original.at[i,'sig'] != 0)]
fig.add_trace(go.Scatter(
    name="Day Start",
    mode="markers", x=x1, y=y1,
    marker=dict(
        size=8,
        color= 'rgb(247,216,37)',
        symbol='diamond-tall'
    )
))
fig.update_xaxes(
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1day", step="day", stepmode="backward"),
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=3, label="3m", step="month", stepmode="backward"),
            dict(step="all")
        ])
    ),
    rangeslider_visible=True,
    rangebreaks=[
        dict(bounds=["sat", "mon"]), #hide weekends
        dict(bounds=[13.75, 8.75], pattern="hour"), #hide hours outside of 8:45am-13:45pm
    ]
)
fig.update_layout(
    title=_title
)
fig.show()
fig.write_html("4_1_fig.html")

#------------------------------------------------------------------------------------------------------

#plot with plotly
import plotly.graph_objects as go
import plotly.io as pio
#pio.renderers.default = 'svg'
pio.renderers.default = 'browser'

original['index'] = signal.index
original = original.set_index('index')
original['sig'] = signal
_title = 'total gains %f, total investment %f%%'%(total_gains, invest)

original['WMA_10'] = talib.WMA(original['Pred'], timeperiod=10)

original2 = original
original2.drop(original2.head(9).index,inplace=True)
original2 = original2.reset_index(drop=True)

fig = go.Figure()

fig.add_trace(go.Scatter(x=original.Date, y=original2.Open, name='Real Track',
                         line=dict(color='firebrick', width=1.2)))

fig.add_trace(go.Scatter(x=original.Date, y=original2.WMA_10, name = 'Prediction_Smooth',
                         line=dict(color='orange', width=1.2)))
fig.add_trace(go.Scatter(x=original.Date, y=original2.Pred, name = 'Prediction',
                         line=dict(color='green', width=1)))
fig.add_trace(go.Scatter(
    name="Buy",
    mode="markers", x=[original2.at[i,'Date'] for i in range(len(original2)) if original2.at[i,'sig'] == 1], y=[original2.at[i,'Open'] for i in range(len(original2)) if original2.at[i,'sig'] == 1],
    marker=dict(
        size=8,
        color= 'rgb(255,0,0)',
        symbol='triangle-down'
    )
))
fig.add_trace(go.Scatter(
    name="Sell",
    mode="markers", x=[original2.at[i,'Date'] for i in range(len(original2)) if original2.at[i,'sig'] == -1], y=[original2.at[i,'Open'] for i in range(len(original2)) if original2.at[i,'sig'] == -1],
    marker=dict(
        size=8,
        color= 'rgb(25,0,0)',
        symbol='triangle-up'
    )
))
x1 = [original2.at[i,'Date'] for i in range(len(original2)) if original2.at[i,'Date'].hour == 9 and original2.at[i,'sig'] != 0]
y1 = [original2.at[i,'Open']+60 for i in range(len(original2)) if (original2.at[i,'Date'].hour == 9 and original2.at[i,'sig'] != 0)]
fig.add_trace(go.Scatter(
    name="Day Start",
    mode="markers", x=x1, y=y1,
    marker=dict(
        size=8,
        color= 'rgb(247,216,37)',
        symbol='diamond-tall'
    )
))
fig.update_xaxes(
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1day", step="day", stepmode="backward"),
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=3, label="3m", step="month", stepmode="backward"),
            dict(step="all")
        ])
    ),
    rangeslider_visible=True,
    rangebreaks=[
        dict(bounds=["sat", "mon"]), #hide weekends
        dict(bounds=[13.75, 8.75], pattern="hour"), #hide hours outside of 8:45am-13:45pm
    ]
)
fig.update_layout(
    title=_title
)
fig.show()
fig.write_html("4_1_smooth_fig.html")'''

